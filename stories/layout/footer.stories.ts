/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, html } from '../utils';

export default {
  title: 'Layout/Footer',
  decorators: [container('container-fluid')],
};

type Story = StoryObj<any>;


export const Footer: Story = {
  render: () => {
    return html`
      <footer class="footer" id="footer">
        <div class="container">
          <div class="footer-end-social-container margin-bottom-30">
            <div class="footer-end-social">
              <p class="footer-end-social-text hidden-xs">Follow Us:</p>
              <ul class="footer-end-social-links list-inline">
                <li>
                  <a class="link-unstyled" href="https://twitter.com/EclipseFdn" title="Twitter account">
                    <span class="fa fa-stack">
                      <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i class="fa fa-twitter fa-stack-1x fa-inverse" aria-hidden="true"></i>
                      <span class="sr-only">Twitter account</span>
                    </span>
                  </a>
                </li>
                <li>
                  <a class="link-unstyled" href="https://www.facebook.com/eclipse.org" title="Facebook account">
                    <span class="fa fa-stack">
                      <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i class="fa fa-facebook fa-stack-1x fa-inverse" aria-hidden="true"></i>
                      <span class="sr-only">Facebook account</span>
                    </span>
                  </a>
                </li>
                <li>
                  <a class="link-unstyled" href="https://www.youtube.com/user/EclipseFdn" title="Youtube account">
                    <span class="fa fa-stack">
                      <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i class="fa fa-youtube-play fa-stack-1x fa-inverse" aria-hidden="true"></i>
                      <span class="sr-only">Youtube account</span>
                    </span>
                  </a>
                </li>
                <li>
                  <a class="link-unstyled" href="https://www.linkedin.com/company/eclipse-foundation" title="Linkedin account">
                    <span class="fa fa-stack">
                      <i class="fa fa-circle fa-stack-2x" aria-hidden="true"></i>
                      <i class="fa fa-linkedin fa-stack-1x fa-inverse" aria-hidden="true"></i>
                      <span class="sr-only">Linkedin account</span>
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="footer-sections row equal-height-md font-bold">
            <div class="col-md-15">
              <div class="row">
                <section id="footer-eclipse-foundation" class="footer-section col-sm-8">
                  <div class="menu-heading">Eclipse Foundation</div>
                  <ul class="nav">
                    <li><a href="https://www.eclipse.org/org/">About</a></li>
                    <li><a href="https://projects.eclipse.org/">Projects</a></li>
                    <li><a href="https://www.eclipse.org/collaborations/">Collaborations</a></li>
                    <li><a href="https://www.eclipse.org/membership/">Membership</a></li>
                    <li><a href="https://www.eclipse.org/sponsor/">Sponsor</a></li>
                  </ul>
                </section>
                <section id="footer-legal" class="footer-section col-sm-8">
                  <div class="menu-heading">Legal</div>
                  <ul class="nav">
                    <li><a href="https://www.eclipse.org/legal/privacy/">Privacy Policy</a></li>
                    <li><a href="https://www.eclipse.org/legal/terms-of-use/">Terms of Use</a></li>
                    <li><a href="https://www.eclipse.org/legal/compliance/">Compliance</a></li>
                    <li><a href="https://www.eclipse.org/org/documents/Community_Code_of_Conduct.php">Code of Conduct</a></li>
                    <li><a href="https://www.eclipse.org/legal/">Legal Resources</a></li>
                    <li><a class="toolbar-manage-cookies" href="#" onclick="event.preventDefault()">Manage Cookies</a></li>
                  </ul>
                </section>
                <section id="footer-more" class="footer-section col-sm-8">
                  <div class="menu-heading">More</div>
                  <ul class="nav">
                    <li><a href="https://www.eclipse.org/security/">Report a Vulnerability</a></li>
                    <li><a href="https://status.eclipse.org/">Service Status</a></li>
                    <li><a href="https://www.eclipse.org/org/foundation/contact.php">Contact Us</a></li>
                    <li><a href="https://www.eclipse.org/projects/support/">Support</a></li>
                  </ul>
                </section>
              </div>
            </div>
            <div id="footer-end" class="footer-section col-md-8 col-md-offset-1 col-sm-24">
              <div class="footer-end-newsletter">
                <form 
                  id="mc-embedded-subscribe-form" 
                  action="https://eclipse.us6.list-manage.com/subscribe/post?u=eaf9e1f06f194eadc66788a85&amp;id=e7538485cd&amp;f_id=00f9c2e1f0" 
                  method="post" 
                  target="_blank"
                  novalidate 
                >
                  <label class="footer-end-newsletter-label" for="email">Subscribe to our Newsletter</label>
                  <div class="footer-end-newsletter-input-wrapper">
                    <input class="footer-end-newsletter-input" type="email" id="email" name="EMAIL" autocomplete="email" placeholder="Enter your email address" />
                    <button class="footer-end-newsletter-submit btn btn-link" id="mc-embedded-subscribe" type="submit" name="subscribe">
                      <i class="fa fa-solid fa-envelope fa-lg" aria-hidden="true"></i>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-sm-24 margin-top-20">
            <div class="row">
              <div id="copyright" class="col-md-16">
                <p id="copyright-text">Copyright © Eclipse Foundation AISBL. All Rights Reserved.</p>
              </div>
            </div>
          </div>
          <a href="#" class="scrollup" style="display: inline; bottom: 100px;">Back to the top</a>
        </div>
      </footer>
    `;
  }
}


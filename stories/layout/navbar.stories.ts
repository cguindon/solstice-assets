/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import eclipsefdnHeader from '../../js/solstice/astro/eclipsefdn.header';
import eclipsefdnMegaMenuPromoContent from '../../js/solstice/eclipsefdn.mega-menu-promo-content';

import type { StoryObj } from '@storybook/html';
import { afterStoryRender, container, html } from '../utils';

export default {
  title: 'Layout/Navigation Bar',
  decorators: [container('container-fluid')],
};

type Story = StoryObj<any>;

const commonOptions: Partial<Story> = {
  argTypes: {
    callForActionText: { 
      control: 'text',
      description: 'The text for the call for action button'
    },
    callForActionIcon: {
      control: 'text',
      description: 'The FontAwesome icon for the call for action button'
    },
    hideCallForAction: {
      control: 'boolean',
    },
    logo: {
      control: 'text',
      description: 'The url to the logo image',
    },
    logoWidth: {
      control: 'number',
    },
  },
  args: {
    logo: 'https://www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-foundation-grey-orange.svg',
    logoWidth: 150,
    callForActionText: 'Download',
    callForActionIcon: 'fa-download',
    hideCallForAction: false,
  },
}

const commonNavbar = ({
  logo,
  logoWidth,
  title,
  callForActionUrl,
  callForActionText,
  callForActionIcon,
  hideCallForAction,
  children
}: any) => html`
  <header class="header-wrapper">
    <div class="header-navbar-wrapper">
      <div class="container">
        <div class="header-navbar">
          <div class="header-navbar-brand">
            <a class="logo-wrapper" href="https://www.eclipse.org/" title="${title}">
              <img src="${logo}" width=${logoWidth} alt="${title}" href="${callForActionUrl}">
            </a>
          </div>
          ${children}
          <div class="header-navbar-end">
            ${!hideCallForAction
              ? html`
                <a class="header-navbar-end-download-btn btn btn-primary" href="${callForActionUrl}">
                  <i class="fa ${callForActionIcon}" aria-hidden="true"></i>
                  ${callForActionText}
                </a>
              `
              : ''}
            <button class="mobile-menu-btn" aria-label="Toggle mobile navigation menu" aria-expanded="false">
              <i class="fa fa-bars fa-xl" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </header>
`;

const directMenu = [
  {
    id: 'projects',
    name: 'Projects',
    href: '#',
  },
  {
    id: 'working-groups',
    name: 'Working Groups',
    href: '#',
  },
  {
    id: 'members',
    name: 'Members',
    href: '#',
  }
];

export const Direct: Story = {
  ...commonOptions,
  render: (args) => {
    return commonNavbar({
      ...args,
      children: html`
        <nav class="header-navbar-nav">
          <ul class="header-navbar-nav-links">
            ${directMenu.map(menuItem => html`
                <li class="navbar-nav-links-item">
                  <a class="link-unstyled" href="${menuItem.href}">${menuItem.name}</a>
                </li>
              `)
              .join('')
            }
          </ul>
        </nav>
      `,
    }); 
  },
}

const dropdownMenu = [
  {
    id: 'projects',
    name: 'Projects',
    href: '#',
    children: [
      {
        id: 'projects-finder',
        name: 'Projects Finder',
        href: '#',
      },
      {
        id: 'projects-activity',
        name: 'Projects Activity',
        href: '#',
      },
      {
        id: 'projects-resources',
        name: 'Projects Resources',
        href: '#',
      },
      {
        id: 'projects-specifications',
        name: 'Specifications',
        href: '#',
      },
      {
        id: 'projects-contribute',
        name: 'Contribute',
        href: '#',
      }
    ]
  },
  {
    id: 'working-groups',
    name: 'Working Groups',
    href: '#',
    children: [],
  },
  {
    id: 'members',
    name: 'Members',
    href: '#',
    children: [],
  }
];

const renderDirectMenuItem = (menuItem: any) => html`<li><a class="link-unstyled" href="${menuItem.href}">${menuItem.name}</a></li>`;

export const Dropdown: Story = {
  ...commonOptions,
  render: (args) => {
    return commonNavbar({
      ...args,
      children: html`
        <nav class="header-navbar-nav">
          <ul class="header-navbar-nav-links">
            ${dropdownMenu.map(menuItem => html`
                <div class="dropdown">
                  ${menuItem.children?.length > 0
                    ? html`
                      <button class="btn-link link-unstyled" id="${menuItem.id}" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${menuItem.name}
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="${menuItem.id}">
                        ${menuItem?.children.map(renderDirectMenuItem).join('')}
                      </ul>
                    `
                    : renderDirectMenuItem(menuItem) 
                  }
                </div>
              `)
              .join('')
            }
          </ul>
        </nav>
      `,
    })
  }
}

export const MegaMenu: Story = {
  render: (_args) => {
    afterStoryRender(() => {
      eclipsefdnHeader.run();
      eclipsefdnMegaMenuPromoContent.render();
    });

    return html`
      <header class="header-wrapper">
        <!-- Navigation Bar -->
        <div class="header-navbar-wrapper">
          <div class="container">
            <div class="header-navbar">
              <div class="header-navbar-brand"><a class="logo-wrapper" href="https://www.eclipse.org/" title="Eclipse Foundation"><img src="https://www.eclipse.org/eclipse.org-common/themes/solstice/public/images/logo/eclipse-foundation-grey-orange.svg" alt="" width="150"></a></div>
              <nav class="header-navbar-nav">
                <ul class="header-navbar-nav-links">
                  <li class="navbar-nav-links-item">
                    <button class="nav-link-js btn-link link-unstyled" type="button" aria-expanded="true" data-menu-target="projects-menu">Projects</button>
                  </li>
                  <li class="navbar-nav-links-item">
                    <button class="nav-link-js btn-link link-unstyled" type="button" aria-expanded="true" data-menu-target="supporters-menu">Supporters</button>
                  </li>
                  <li class="navbar-nav-links-item">
                    <button class="nav-link-js btn-link link-unstyled" type="button" aria-expanded="true" data-menu-target="collaborations-menu">Collaborations</button>
                  </li>
                  <li class="navbar-nav-links-item">
                    <button class="nav-link-js btn-link link-unstyled" type="button" aria-expanded="true" data-menu-target="resources-menu">Resources</button>
                  </li>
                  <li class="navbar-nav-links-item">
                    <button class="nav-link-js btn-link link-unstyled" type="button" aria-expanded="true" data-menu-target="the-foundation-menu">The Foundation</button>
                  </li>
                </ul>
              </nav>
              <div class="header-navbar-end">
                <a class="header-navbar-end-download-btn btn btn-primary" href="https://www.eclipse.org/downloads/">
                  <i class="fa" aria-hidden="true"></i> Download
                </a>
                <button class="mobile-menu-btn" aria-label="Toggle mobile navigation menu" aria-expanded="false">
                <i class="fa fa-bars fa-xl"></i></button>
              </div>
            </div>
          </div>
        </div>
        <!-- Mobile Menu -->
        <nav class="mobile-menu hidden" aria-expanded="false">
          <ul>
            <li class="mobile-menu-dropdown">
              <a class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="projects-menu"><span>Projects</span>
              <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
              <div class="mobile-menu-sub-menu-wrapper">
                <ul class="mobile-menu-sub-menu hidden" id="projects-menu">
                  <li class="mobile-menu-dropdown">
                    <a data-target="projects-technologies-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Technologies</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="projects-technologies-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/topics/ide/">Developer Tools &amp; IDEs</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/topics/cloud-native/">Cloud Native</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/topics/edge-and-iot">Edge &amp; IoT</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/topics/automotive-and-mobility/">Automotive &amp; Mobility</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="mobile-menu-dropdown">
                    <a data-target="projects-projects-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Projects</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="projects-projects-sub-menu">
                        <li><a class="mobile-menu-item" href="https://projects.eclipse.org/">Project Finder</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/projects/project_activity.php">Project Activity</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/projects/resources">Project Resources</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/specifications">Specifications</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/contribute/">Contribute</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
            <li class="mobile-menu-dropdown">
              <a class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="supporters-menu"><span>Supporters</span>
              <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
              <div class="mobile-menu-sub-menu-wrapper">
                <ul class="mobile-menu-sub-menu hidden" id="supporters-menu">
                  <li class="mobile-menu-dropdown">
                    <a data-target="supporters-membership-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Membership</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="supporters-membership-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/membership/explore-membership/">Our Members</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/membership/">Member Benefits</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/membership/#tab-levels">Membership Levels &amp; Fees</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/membership/#tab-membership">Membership Application</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/membership/#tab-resources">Member Resources</a></li>
                        <li><a class="mobile-menu-item" href="https://membership.eclipse.org/portal">Member Portal</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="mobile-menu-dropdown">
                    <a data-target="supporters-sponsorship-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Sponsorship</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="supporters-sponsorship-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/sponsor/">Sponsor</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/corporate_sponsors">Corporate Sponsorship</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/sponsor/collaboration/">Sponsor a Collaboration</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
            <li class="mobile-menu-dropdown">
              <a class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="collaborations-menu"><span>Collaborations</span>
              <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
              <div class="mobile-menu-sub-menu-wrapper">
                <ul class="mobile-menu-sub-menu hidden" id="collaborations-menu">
                  <li class="mobile-menu-dropdown">
                    <a data-target="collaborations-industry-collaborations-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Industry Collaborations</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="collaborations-industry-collaborations-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/collaborations/">About Industry Collaborations</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/workinggroups/explore.php">Current Collaborations</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/workinggroups/about.php">About Working Groups</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/collaborations/interest-groups/">About Interest Groups</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="mobile-menu-dropdown">
                    <a data-target="collaborations-research-collaborations-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Research Collaborations</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="collaborations-research-collaborations-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/research/">Research @ Eclipse</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
            <li class="mobile-menu-dropdown">
              <a class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="resources-menu"><span>Resources</span>
              <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
              <div class="mobile-menu-sub-menu-wrapper">
                <ul class="mobile-menu-sub-menu hidden" id="resources-menu">
                  <li class="mobile-menu-dropdown">
                    <a data-target="resources-open-source-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Open Source for Business</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="resources-open-source-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/value/">Business Value of Open Source</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/os4biz/services/">Professional Services</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/os4biz/ospo/">Open Source Program Offices</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="mobile-menu-dropdown">
                    <a data-target="resources-happening-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>What's Happening</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="resources-happening-sub-menu">
                        <li><a class="mobile-menu-item" href="https://newsroom.eclipse.org/">News</a></li>
                        <li><a class="mobile-menu-item" href="https://events.eclipse.org/">Events</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/community/eclipse_newsletter/">Newsletter</a></li>
                        <li><a class="mobile-menu-item" href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/foundation/eclipseawards/">Awards &amp; Recognition</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="mobile-menu-dropdown">
                    <a data-target="resources-developer-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Developer Resources</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="resources-developer-sub-menu">
                        <li><a class="mobile-menu-item" href="https://eclipse.org/forums/">Forum</a></li>
                        <li><a class="mobile-menu-item" href="https://accounts.eclipse.org/mailing-list">Mailing Lists</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/blogs-and-videos/">Blogs &amp; Videos</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/resources/marketplaces/">Marketplaces</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
            <li class="mobile-menu-dropdown">
              <a class="mobile-menu-item mobile-menu-dropdown-toggle" data-target="the-foundation-menu"><span>The Foundation</span>
              <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
              <div class="mobile-menu-sub-menu-wrapper">
                <ul class="mobile-menu-sub-menu hidden" id="the-foundation-menu">
                  <li class="mobile-menu-dropdown">
                    <a data-target="the-foundation-about-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>About</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="the-foundation-about-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/">About the Eclipse Foundation</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/governance">Board &amp; Governance</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/foundation/staff/">Staff</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/services">Services</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="mobile-menu-dropdown">
                    <a data-target="the-foundation-legal-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>Legal</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="the-foundation-legal-sub-menu">
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/legal/">Legal Policies</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/legal/privacy/">Privacy Policy</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/legal/terms-of-use/">Terms of Use</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/legal/compliance">Compliance</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/legal/epl-2.0/">Eclipse Public License</a></li>
                      </ul>
                    </div>
                  </li>
                  <li class="mobile-menu-dropdown">
                    <a data-target="the-foundation-more-sub-menu" class="mobile-menu-item mobile-menu-dropdown-toggle" aria-expanded="false"><span>More</span>
                    <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="mobile-menu-sub-menu-wrapper">
                      <ul class="mobile-menu-sub-menu mobile-menu-links-menu hidden" id="the-foundation-more-sub-menu">
                        <li><a class="mobile-menu-item" href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/careers/">Careers</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/artwork/">Logos &amp; Artwork</a></li>
                        <li><a class="mobile-menu-item" href="https://www.eclipse.org/org/foundation/contact.php">Contact Us</a></li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </nav>
        <!-- Mega Menu -->
        <div class="eclipsefdn-mega-menu">
          <div class="mega-menu-submenu container hidden" data-menu-id="projects-menu">
            <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">Projects</p>
              <p class="mega-menu-submenu-featured-story-text">
                The Eclipse Foundation is home to the Eclipse IDE, Jakarta EE,
                and hundreds of open source projects, including runtimes, tools,
                specifications, and frameworks for cloud and edge applications,
                IoT, AI, automotive, systems engineering, open processor
                designs, and many others.
              </p>
            </div>
            <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Technologies</p>
                <ul>
                  <li><a href="https://www.eclipse.org/topics/ide/">Developer Tools &amp; IDEs</a></li>
                  <li><a href="https://www.eclipse.org/topics/cloud-native/">Cloud Native</a></li>
                  <li><a href="https://www.eclipse.org/topics/edge-and-iot">Edge &amp; IoT</a></li>
                  <li><a href="https://www.eclipse.org/topics/automotive-and-mobility/">Automotive &amp; Mobility</a></li>
                </ul>
              </div>
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Projects</p>
                <ul>
                  <li><a href="https://projects.eclipse.org/">Project Finder</a></li>
                  <li><a href="https://www.eclipse.org/projects/project_activity.php">Project Activity</a></li>
                  <li><a href="https://www.eclipse.org/projects/resources">Project Resources</a></li>
                  <li><a href="https://www.eclipse.org/specifications">Specifications</a></li>
                  <li><a href="https://www.eclipse.org/contribute/">Contribute</a></li>
                </ul>
              </div>
            </div>
            <div class="mega-menu-submenu-ad-wrapper">
              <div class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content" data-ad-format="ads_square" data-ad-publish-to="eclipse_org_home"></div>
            </div>
          </div>
          <div class="mega-menu-submenu container hidden" data-menu-id="supporters-menu">
            <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">Supporters</p>
              <p class="mega-menu-submenu-featured-story-text">
                The Eclipse Foundation is an international non-profit association
                supported by our members, including industry leaders who value
                open source as a key enabler for their business strategies.
              </p>
            </div>
            <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Membership</p>
                <ul>
                  <li><a href="https://www.eclipse.org/membership/explore-membership/">Our Members</a></li>
                  <li><a href="https://www.eclipse.org/membership/">Member Benefits</a></li>
                  <li><a href="https://www.eclipse.org/membership/#tab-levels">Membership Levels &amp; Fees</a></li>
                  <li><a href="https://www.eclipse.org/membership/#tab-membership">Membership Application</a></li>
                  <li><a href="https://www.eclipse.org/membership/#tab-resources">Member Resources</a></li>
                  <li><a href="https://membership.eclipse.org/portal">Member Portal</a></li>
                </ul>
              </div>
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Sponsorship</p>
                <ul>
                  <li><a href="https://www.eclipse.org/sponsor/">Sponsor</a></li>
                  <li><a href="https://www.eclipse.org/org/corporate_sponsors">Corporate Sponsorship</a></li>
                  <li><a href="https://www.eclipse.org/sponsor/collaboration/">Sponsor a Collaboration</a></li>
                </ul>
              </div>
            </div>
            <div class="mega-menu-submenu-ad-wrapper">
              <div class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content" data-ad-format="ads_square" data-ad-publish-to="eclipse_org_home"></div>
            </div>
          </div>
          <div class="mega-menu-submenu container hidden" data-menu-id="collaborations-menu">
            <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">Collaborations</p>
              <p class="mega-menu-submenu-featured-story-text">
                Whether you intend on contributing to Eclipse technologies that
                are important to your product strategy, or simply want to explore
                a specific innovation area with like-minded organisations, the
                Eclipse Foundation is the open source home for industry
                collaboration.
              </p>
            </div>
            <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Industry Collaborations</p>
                <ul>
                  <li><a href="https://www.eclipse.org/collaborations/">About Industry Collaborations</a></li>
                  <li><a href="https://www.eclipse.org/org/workinggroups/explore.php">Current Collaborations</a></li>
                  <li><a href="https://www.eclipse.org/org/workinggroups/about.php">About Working Groups</a></li>
                  <li><a href="https://www.eclipse.org/collaborations/interest-groups/">About Interest Groups</a></li>
                </ul>
              </div>
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Research Collaborations</p>
                <ul>
                  <li><a href="https://www.eclipse.org/research/">Research @ Eclipse</a></li>
                </ul>
              </div>
            </div>
            <div class="mega-menu-submenu-ad-wrapper">
              <div class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content" data-ad-format="ads_square" data-ad-publish-to="eclipse_org_home"></div>
            </div>
          </div>
          <div class="mega-menu-submenu container hidden" data-menu-id="resources-menu">
            <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">Resources</p>
              <p class="mega-menu-submenu-featured-story-text">
                The Eclipse community consists of individual developers and
                organisations spanning many industries. Stay up to date on our
                open source community and find resources to support your journey.
              </p>
            </div>
            <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Open Source for Business</p>
                <ul>
                  <li><a href="https://www.eclipse.org/org/value/">Business Value of Open Source</a></li>
                  <li><a href="https://www.eclipse.org/os4biz/services/">Professional Services</a></li>
                  <li><a href="https://www.eclipse.org/os4biz/ospo/">Open Source Program Offices</a></li>
                </ul>
              </div>
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">What's Happening</p>
                <ul>
                  <li><a href="https://newsroom.eclipse.org/">News</a></li>
                  <li><a href="https://www.eclipse.org/events/">Events</a></li>
                  <li><a href="https://www.eclipse.org/community/eclipse_newsletter/">Newsletter</a></li>
                  <li><a href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a></li>
                  <li><a href="https://www.eclipse.org/org/foundation/eclipseawards/">Awards &amp; Recognition</a></li>
                </ul>
              </div>
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Developer Resources</p>
                <ul>
                  <li><a href="https://www.eclipse.org/forums/">Forum</a></li>
                  <li><a href="https://accounts.eclipse.org/mailing-list">Mailing Lists</a></li>
                  <li><a href="https://www.eclipse.org/blogs-and-videos/">Blogs &amp; Videos</a></li>
                  <li><a href="https://www.eclipse.org/resources/marketplaces/">Marketplaces</a></li>
                </ul>
              </div>
            </div>
            <div class="mega-menu-submenu-ad-wrapper">
              <div class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content" data-ad-format="ads_square" data-ad-publish-to="eclipse_org_home"></div>
            </div>
          </div>
          <div class="mega-menu-submenu container hidden" data-menu-id="the-foundation-menu">
            <div class="mega-menu-submenu-featured-story">
              <p class="mega-menu-submenu-featured-story-heading">The Foundation</p>
              <p class="mega-menu-submenu-featured-story-text">The Eclipse Foundation provides our global community of individuals and
                organisations with a mature, scalable, and vendor-neutral environment for
                open source software collaboration and innovation.
              </p>
            </div>
            <div class="mega-menu-submenu-links-section">
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">About</p>
                <ul>
                  <li><a href="https://www.eclipse.org/org/">About the Eclipse Foundation</a></li>
                  <li><a href="https://www.eclipse.org/org/governance">Board &amp; Governance</a></li>
                  <li><a href="https://www.eclipse.org/org/foundation/staff/">Staff</a></li>
                  <li><a href="https://www.eclipse.org/org/services">Services</a></li>
                </ul>
              </div>
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">Legal</p>
                <ul>
                  <li><a href="https://www.eclipse.org/legal/">Legal Policies</a></li>
                  <li><a href="https://www.eclipse.org/legal/privacy/">Privacy Policy</a></li>
                  <li><a href="https://www.eclipse.org/legal/terms-of-use/">Terms of Use</a></li>
                  <li><a href="https://www.eclipse.org/legal/compliance">Compliance</a></li>
                  <li><a href="https://www.eclipse.org/legal/epl-2.0/">Eclipse Public License</a></li>
                </ul>
              </div>
              <div class="mega-menu-submenu-links">
                <p class="menu-heading">More</p>
                <ul>
                  <li><a href="https://newsroom.eclipse.org/news/press-releases">Press Releases</a></li>
                  <li><a href="https://www.eclipse.org/careers/">Careers</a></li>
                  <li><a href="https://www.eclipse.org/org/artwork/">Logos &amp; Artwork</a></li>
                  <li><a href="https://www.eclipse.org/org/foundation/contact.php">Contact Us</a></li>
                </ul>
              </div>
            </div>
            <div class="mega-menu-submenu-ad-wrapper">
              <div class="eclipsefdn-mega-menu-promo-content mega-menu-promo-content" data-ad-format="ads_square" data-ad-publish-to="eclipse_org_home"></div>
            </div>
          </div>
        </div>
      </header>
    `;
  }
}

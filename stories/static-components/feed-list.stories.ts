/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import type { Meta, StoryObj } from '@storybook/html';
import { container, html, setOptionalClass } from '../utils';

const meta: Meta = {
  title: 'Static Components/Feed List',
  tags: ['hugo shortcode'],
  decorators: [container()],
};

export default meta;

type Story = StoryObj<any>;

export const FeedList: Story = {
  argTypes: {
    direction: {
      control: {
        type: 'radio',
        labels: {
          'feed-list-horizontal': 'horizontal',
          'feed-list-vertical': 'vertical',
        }
      },
      options: ['feed-list-vertical', 'feed-list-horizontal'],
    },
    bordered: {
      control: 'boolean',
    },
    shadowed: {
      control: 'boolean',
      if: {
        arg: 'gapped',
        truthy: true,
      },
    },
    gapped: {
      control: 'boolean',
    },
  },
  args: {
    bordered: false,
    gapped: false,
    direction: 'feed-list-vertical',
  },
  render: ({ bordered, direction, gapped, shadowed }) => {
    const columnClass = getColumnClass(direction);

    return html`
      <div class="row">
        <div class="${columnClass}">
          <!-- Feed List -->
          <div class="feed-list ${direction} ${setOptionalClass('feed-list-bordered', bordered)} ${setOptionalClass('feed-list-gapped', gapped)} ${setOptionalClass('feed-list-shadowed', shadowed)}">
            <div class="feed-actions">
              <img 
                class="feed-actions-img" 
                src="https://www.eclipse.org/images/redesign-stock/community-news.jpg" 
                alt=""
              >
              <div class="feed-actions-btns">
                <a class="btn btn-secondary" href="#">Subscribe</a>
                <a class="btn btn-primary" href="#">View All</a>
              </div>
            </div>
            <div class="feed-list-items">
              ${ Array.from({ length: 4 }).map(() => {
                return html`
                  <a class="feed-item padding-20 link-unstyled" href="#">
                    <p class="fw-700 margin-bottom-5">Lorem Ipsum Dolor Sit Amet</p>
                    <p class="fs-xs margin-bottom-10 uppercase fw-600 text-primary">1 November 2024</p>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                      sit amet ex in dui tempor pretium. Quisque aliquam ligula
                      in tellus finibus scelerisque.
                    </p>
                  </a>
                `;
              }).join('')}
            </div>
          </div>
          <!-- Feed List end -->
        </div>
      </div>
    `;
  },
}

/** Retrieves the column class best suited to demo the directional variant for
  * feed list. */
const getColumnClass = (direction: 'feed-list-horizontal' | 'feed-list-vertical') => {
  switch (direction) {
    case 'feed-list-vertical':
      return 'col-sm-8 col-sm-offset-8';
    case 'feed-list-horizontal':
      return 'col-xs-24';
    default:
      return '';
  }
}

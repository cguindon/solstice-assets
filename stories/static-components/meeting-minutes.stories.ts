/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import type { Meta, StoryObj } from '@storybook/html';
import '../../js/bootstrap';
import { container } from '../utils';

const meta: Meta = {
  title: 'Static Components/Meeting Minutes',
  tags: ['hugo shortcode'],
  decorators: [container()],
};

//@todo: display tags in docs page and link to hugo shortcode doc page.

export default meta;

type Story = StoryObj<any>;

export const MeetingMinutes: Story = {
  render: () => {
    return (`
<div class="eclipsefdn-meeting-minutes">
    <ul class="meeting-minutes-tab nav nav-tabs link-unstyled solstice-tabs" role="tablist">
        <li role="presentation" class="meeting-minutes-tab-item active"><a target="_self" class="meeting-minutes-tab-item-link" href="#marketing_committee" aria-controls="marketing_committee" role="tab" data-toggle="tab">Marketing Committee</a></li>
        <li role="presentation" class="meeting-minutes-tab-item"><a target="_self" class="meeting-minutes-tab-item-link" href="#specification_committee" aria-controls="specification_committee" role="tab" data-toggle="tab">Specification Committee</a></li>
        <li role="presentation" class="meeting-minutes-tab-item"><a target="_self" class="meeting-minutes-tab-item-link" href="#steering_committee" aria-controls="steering_committee" role="tab" data-toggle="tab">Steering Committee</a></li>
    </ul>
  <div class="tab-content row">
        <div role="tabpanel" class="tab-pane active" id="marketing_committee">
          <div class="card-container col-sm-24">
             <div class="glyph-highlight left-align"><div class="glyph-container text">2024</div><div class="glyph-bottom"></div></div>
            <div class="card-panel bordered panel panel-default with-glyph">
              <div class="panel-body">
                <ul class="meeting-minutes-list tri-col margin-bottom-0">
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0321-2024.pdf">
                        21 March 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0307-2024.pdf">
                        7 March 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0222-2024.pdf">
                        22 February 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0208-2024.pdf">
                        8 February 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0125-2024.pdf">
                        25 January 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0111-2024.pdf">
                        11 January 2024 (pdf)
                      </a>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
          <div class="card-container col-sm-24">
             <div class="glyph-highlight left-align"><div class="glyph-container text">2023</div><div class="glyph-bottom"></div></div>
            <div class="card-panel bordered panel panel-default with-glyph">
              <div class="panel-body">
                <ul class="meeting-minutes-list tri-col margin-bottom-0">
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-1214-2023.pdf">
                        14 December 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-1130-2023.pdf">
                        30 November 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-1116-2023.pdf">
                        16 November 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-1102-2023.pdf">
                        2 November 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-1012-2023.pdf">
                        12 October 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-1005-2023.pdf">
                        5 October 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0921-2023.pdf">
                        21 September 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0907-2023.pdf">
                        7 September 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0824-2023.pdf">
                        24 August 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0810-2023.pdf">
                        10 August 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0627-2023.pdf">
                        27 July 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0713-2023.pdf">
                        13 July 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0629-2023.pdf">
                        29 June 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0615-2023.pdf">
                        15 June 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0601-2023.pdf">
                        1 June 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0518-2023.pdf">
                        18 May 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0504-2023.pdf">
                        4 May 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0420-2023.pdf">
                        20 April 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0413-2023.pdf">
                        13 April 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0330-2023.pdf">
                        30 March 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0323-2023.pdf">
                        23 March 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0309-2023.pdf">
                        9 March 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0223-2023.pdf">
                        23 February 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0209-2023.pdf">
                        9 February 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0126-2023.pdf">
                        26 January 2023 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/marketing_committee/minutes-marketing-0112-2023.pdf">
                        12 January 2023 (pdf)
                      </a>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
      </div>
        <div role="tabpanel" class="tab-pane" id="specification_committee">
          <div class="card-container col-sm-24">
             <div class="glyph-highlight left-align"><div class="glyph-container text">2024</div><div class="glyph-bottom"></div></div>
            <div class="card-panel bordered panel panel-default with-glyph">
              <div class="panel-body">
                <ul class="meeting-minutes-list tri-col margin-bottom-0">
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/specification_committee/minutes-specification-march-20-2024.pdf">
                        20 March 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/specification_committee/minutes-specification-march-6-2024.pdf">
                        6 March 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/specification_committee/minutes-specification-february-21-2024.pdf">
                        21 February 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/specification_committee/minutes-specification-february-7-2024.pdf">
                        7 February 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/specification_committee/minutes-specification-january-24-2024.pdf">
                        24 January 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/specification_committee/minutes-specification-january-10-2024.pdf">
                        10 January 2024 (pdf)
                      </a>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
      </div>
        <div role="tabpanel" class="tab-pane" id="steering_committee">
          <div class="card-container col-sm-24">
             <div class="glyph-highlight left-align"><div class="glyph-container text">2024</div><div class="glyph-bottom"></div></div>
            <div class="card-panel bordered panel panel-default with-glyph">
              <div class="panel-body">
                <ul class="meeting-minutes-list tri-col margin-bottom-0">
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/steering_committee/minutes-march-26-2024.pdf">
                        26 March 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/steering_committee/minutes-march-12-2024.pdf">
                        12 March 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/steering_committee/minutes-february-27-2024.pdf">
                        27 February 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/steering_committee/minutes-february-13-2024.pdf">
                        13 February 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/steering_committee/minutes-january-30-2024.pdf">
                        30 January 2024 (pdf)
                      </a>
                    </li>
                    <li>
                      <a class="link-unstyled" href="https://jakarta.ee/about/meeting_minutes/steering_committee/minutes-january-16-2024.pdf">
                        16 January 2024 (pdf)
                      </a>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
      </div>
  </div>
</div>
    `)
  },
}

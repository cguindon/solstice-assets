/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import type { StoryObj } from '@storybook/html';
import { container } from '../utils';

export default {
  title: 'Core/Buttons',
  decorators: [container('container text-center')],
};

type Story = StoryObj<any>;

export const Button: Story = {
  argTypes: {
    variant: { control: 'select', options: ['default', 'primary', 'secondary', 'neutral', 'warning', 'success', 'danger', 'link'] },
    size: { control: 'radio', options: ['unset', 'xs', 'sm', 'lg'], },
    block: { control: 'boolean' },
    textContent: { control: 'text' },
  },
  args: {
    textContent: 'Button',
    variant: 'primary',
    block: false,
  },
  render: ({ block, textContent, variant, size }) => {
    return (`
      <button 
        class="btn btn-${variant}${size && size !== 'unset' ? ` btn-${size}` : ''}${block ? ' btn-block' : ''}"
        type="button" 
      >
        ${textContent}
      </button>
    `)
  },
}


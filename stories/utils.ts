/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export const workingGroups = [
  'internet-things-iot',
  'sdv',
  'jakarta-ee',
  'open-vsx',
  'dataspace',
  'openhw-europe',
  'openmdm',
  'opengenesis',
  'eclipse-ide',
  'ecri',
  'cloud-development-tools',
  'microprofile',
  'osgi',
  'openpass',
  'oniro',
  'sparkplug',
  'asciidoc',
  'openhw-asia',
  'adoptium',
  'orc',
].sort();

export const interestGroups = [
  'openmobility',
  'models4privacy',
  'threadx-ig',
].sort();

export const publishTargets = [
  'eclipse_org', 
  'eclipse_iot', 
  'sparkplug', 
  'jakarta_ee', 
  'research', 
  'edge_native', 
  'openhwgroup', 
  'openmdm', 
  'openmobility', 
  'openpass', 
  'osgi', 
  'eclipse_ide', 
  'asciidoc', 
  'ospo_zone', 
  'oniro', 
  'sdv', 
  'adoptium'
].sort();

/**
  * Outputs a attribute into an html template if the value is truthy.
  *
  * @param attributeName - the name of the attribute to add
  * @param value - the value of the attribute
  * @returns html attribute as a string
  */
export const setOptionalAttribute = (attributeName: string, value?: string) => {
  return value ? `${attributeName}="${value}"` : '';
}

/** 
  * Adds a class if the param is truthy. Otherwise, an empty string. 
  *
  * @param className - the name of the class to add
  * @param truthy - a boolean which, if true, will return the class
  * @returns the class or an empty string depending on the value of truthy.
  */
export const setOptionalClass = (className: string, truthy: boolean) => {
  return truthy ? className : '';
}

/**
  * Runs the callback after a story renders
  *
  * @param callbackFn - the callback function to run 
  */ 
export const afterStoryRender = (callbackFn: () => any): void => {
  setTimeout(() => callbackFn(), 10)
}

/** Removes empty lines from a template string. */
export const html = (strings: TemplateStringsArray, ...values: any[]): string => {
  // Build a single string from the array of template strings.
  let result = strings.reduce((acc, str, i) => acc + str + (values[i] || ''), '');
  // Remove empty lines.
  return result.replace(/^\s*[\r\n]/gm, '');
};

/** A storybook decorator to set the container class of the story. */
export const container = (containerClass: string = 'container') => {
  return (Story: any) => html`
    <div class="${containerClass}" id="container">
      ${Story()}
    </div>
  `;
}

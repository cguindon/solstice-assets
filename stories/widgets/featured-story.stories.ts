/*
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { afterStoryRender, container, setOptionalAttribute, publishTargets } from '../utils';
import '../../js/api/jquery.eclipsefdn.api';

export default {
  title: 'Widgets/Featured Story',
  decorators: [container('container-fluid')],
};

type Story = StoryObj<any>;

export const Default: Story = {
  argTypes: {
    id: { control: 'text' },
    publishTarget: { control: 'select', options: publishTargets },
  },
  args: {
    id: 'featured-story',
    publishTarget: 'eclipse_org',
  },
  render: ({ id, publishTarget }) => {
    return (`
      <div 
        class="eclipsefdn-featured-story featured-story" 
        id="${id}"
        ${setOptionalAttribute('data-publish-target', publishTarget)}
      >
        <div class="container">
          <div class="row">
            <div class="col-sm-24 featured-container">
            </div>
          </div>
        </div>
      </div>

      <script>
        (function(e) {
          e('#${id}').eclipseFdnApi({ type: 'featuredStory' }) 
        })(jQuery, document)
      </script>
    `)
  },
}

export const Section: Story = {
  argTypes: {
    id: { control: 'text' },
    publishTarget: { control: 'select', options: publishTargets },
  },
  args: {
    id: 'featured-story',
    publishTarget: 'eclipse_org',
  },
  render: ({ id, publishTarget }) => {
    // Mimic jquery-match-height for the sake of the story demo.
    const style = document.createElement('style');
    style.innerHTML = `
      @media (min-width: 992px) {
        .featured-story > .container > .row {
          display: flex;
        }
      }
    `;
    document.body.appendChild(style);

    return (`
      <div 
        class="eclipsefdn-featured-story featured-story" 
        id="${id}"
        ${setOptionalAttribute('data-publish-target', publishTarget)}
      >
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-sm-offset-2 col-md-16 col-md-offset-0 featured-story-block featured-story-block-content match-height-item-by-row featured-container">
            </div>
            <div class="col-sm-9 col-md-8 featured-side featured-story-block match-height-item-by-row">
              <div class="featured-side-content text-center">
                <p>
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="none" stroke="currentcolor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail">
                    <path d="M4 4h16c1.1.0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1.0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                    <polyline points="22,6 12,13 2,6"></polyline>
                  </svg>
                </p>
                <p>Sign up to our<br>Newsletter</p>
                <form action="https://eclipsecon.us6.list-manage.com/subscribe/post" method="post" target="_blank">
                  <div class="form-group">
                    <input type="hidden" name="u" value="eaf9e1f06f194eadc66788a85">
                    <input type="hidden" name="id" value="98ae69e304">
                  </div>
                  <input type="submit" value="Subscribe" name="subscribe" class="button btn btn-primary">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <script>
        (function(e) {
          e('#${id}').eclipseFdnApi({ type: 'featuredStory' }) 
        })(jQuery, document)
      </script>
    `)
  },
}

/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, afterStoryRender, setOptionalAttribute } from '../utils';
import eclipsefdnVideoList from '../../js/solstice/eclipsefdn.video-list';

export default {
  title: 'Widgets/Video List',
  decorators: [container()],
};

type Story = StoryObj<any>;

const commonOptions: Partial<Story> = {
  argTypes: {
    playlistIds: { control: 'text', description: 'Comma separated playlist IDs from YouTube' },
    descriptionMaxLength: { control: 'number' },
  },
  args: {
    playlistIds: 'PLutlXcN4EAwDxs85DUnmbnWov3SusY9mG, PLutlXcN4EAwDDwfoO67BY6KUPYVct8BIq, PLy7t4z5SYNaSruNciCsq79vfquXnZ7iTi',
    descriptionMaxLength: 200,
  },
}

export const VideoList: Story = {
  ...commonOptions,
  render: ({ playlistIds, descriptionMaxLength }) => {
    afterStoryRender(eclipsefdnVideoList.render);

    return (`
      <div 
        class="eclipsefdn-video-list"
        data-playlist-ids="${playlistIds}"
        ${setOptionalAttribute('data-description-max-length', descriptionMaxLength)}
      >
      </div>
    `)
  },
}


/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, setOptionalAttribute, publishTargets } from '../utils';
import '../../js/api/jquery.eclipsefdn.api';

export default {
  title: 'Widgets/Featured Footer',
  decorators: [container('container-fluid')],
};

type Story = StoryObj<any>;

export const FeaturedFooter: Story = {
  argTypes: {
    id: { control: 'text' },
    publishTarget: { control: 'select', options: publishTargets },
  },
  args: {
    id: 'featured-footer',
    publishTarget: 'eclipse_org',
  },
  render: ({ id, publishTarget }) => {
    return (`
      <div 
        class="eclipsefdn-featured-footer featured-footer" 
        id="${id}"
        ${setOptionalAttribute('data-publish-target', publishTarget)}
      >
        <div class="container">
          <div class="row">
            <div class="col-sm-24 featured-container">
            </div>
          </div>
        </div>
      </div>

      <script>
        (function(e) {
          e('#${id}').eclipseFdnApi({ type: 'featuredFooter' }) 
        })(jQuery, document)
      </script>
    `)
  },
}

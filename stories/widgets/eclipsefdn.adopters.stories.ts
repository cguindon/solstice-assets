/*
 * Copyright (c) 2025 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, afterStoryRender, workingGroups } from '../utils';
import eclipsefdnAdopters from '../../js/solstice/eclipsefdn.adopters';

export default {
  title: 'Widgets/Adopters',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const Adopters: Story = {
  argTypes: {
    workingGroup: { 
      control: 'select', 
      options: workingGroups,
    },
    logoWhite: {
      control: 'boolean',
    },
  },
  args: {
    workingGroup: 'internet-things-iot',
    logoWhite: false,
  },
  render: ({ workingGroup, logoWhite }) => {
    afterStoryRender(async () => {
      await eclipsefdnAdopters.render();

      const container = document.getElementById('container') as HTMLDivElement;
      const h2s = container.querySelectorAll('h2');

      if (logoWhite) {
        // Create a dark mode version of the page for the sake of the demo.
        document.body.style.backgroundColor = '#333';
        h2s.forEach((h2) => h2.style.color = '#fff');
      } else {
        // Remove inline styles to unapply the dark mode.
        document.body.style.removeProperty('background-color');        
        h2s.forEach((h2) => h2.style.removeProperty('color'));
      }
    });

    return (`
      <div 
        class="eclipsefdn-adopters" 
        data-working-group="${workingGroup}"
        ${logoWhite ? 'data-logo-white="true"' : ''}
      ></div>
    `);
  },
}



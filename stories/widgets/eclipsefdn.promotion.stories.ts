/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, afterStoryRender, setOptionalAttribute } from '../utils';
import eclipsefdnPromotion from '../../js/solstice/eclipsefdn.promotion';

export default {
  title: 'Widgets/Promotion',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const Promotion: Story = {
  argTypes: {
    adPublishTo: { 
      control: 'select', 
      options: [
        'eclipse_org_home',
        'eclipse_org_downloads',
      ],
    },
    adFormat: {
      control: 'select',
      options: [
        'ads_square', 
        'ads_top_leaderboard',
      ],
    },
  },
  args: {
    adPublishTo: 'eclipse_org_home',
    adFormat: 'ads_square',
  },
  render: ({ adFormat, adPublishTo }) => {
    afterStoryRender(eclipsefdnPromotion.render);

    return (`
      <div 
        class="eclipsefdn-promo-content" 
        data-ad-format="${adFormat}"
        data-ad-publish-to="${adPublishTo}"
      ></div>
    `);
  },
}


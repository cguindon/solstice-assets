/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, afterStoryRender } from '../utils';
import eclipsefdnParticipatingOrganizations from '../../js/solstice/eclipsefdn.participating-organizations';

export default {
  title: 'Widgets/Participating Organizations',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const ParticipatingOrganizations: Story = {
  argTypes: {
    projectShortId: { control: 'text' },
  },
  args: {
    projectShortId: 'automotive.tractusx',
  },
  render: ({ projectShortId }) => {
    afterStoryRender(eclipsefdnParticipatingOrganizations.render);
    return (`
      <div
        class="eclipsefdn-participating-organizations"
        data-project-short-id="${projectShortId}"
      ></div>
    `);
  },
};


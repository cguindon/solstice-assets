/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container } from '../utils';
import '../../js/api/jquery.eclipsefdn.api';

export default {
  title: 'Widgets/Projects List',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const ProjectsList: Story = {
  argTypes: {
    id: { control: 'text' },
    username: { control: 'text' },
    currentUser: { control: 'text' },
    apiUrl: { control: 'text' },
    contentPlaceholder: { control: 'text' },
  },
  args: {
    id: 'projects-list',
    currentUser: 'webdev',
    username: 'cguindon',
    apiUrl: 'https://api.eclipse.org',
    contentPlaceholder: '#projects-list',
  },
  render: ({ id, username, currentUser, apiUrl, contentPlaceholder }) => {
    return (`
      <div 
        id="${id}" 
      >
      </div>

      <script>
        (function($) {
          $('#${id}').eclipseFdnApi({ 
            type: 'projectsList',
            username: '${username}',
            currentUser: '${currentUser}',
            apiUrl: '${apiUrl}',
            contentPlaceholder: $('${contentPlaceholder}'),
          }) 
        })(jQuery, document)
      </script>
    `)
  },
}

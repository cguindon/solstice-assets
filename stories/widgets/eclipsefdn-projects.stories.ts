/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';
import '../../js/solstice/eclipsefdn.match-height';
import type { Meta, StoryObj } from '@storybook/html';
import { container, afterStoryRender, setOptionalAttribute, workingGroups, html } from '../utils';
import eclipsefdnProjects from '../../js/solstice/eclipsefdn.projects';

const meta: Meta = {
  title: 'Widgets/Projects',
  tags: ['autodocs'],
  decorators: [container()],
};

export default meta;

type Story = StoryObj<any>;

const commonOptions: Partial<Story> = {
  argTypes: {
    workingGroup: { 
      control: 'select', 
      options: [...workingGroups],
      description: 'The target working group to display projects for.',
      if: { 
        arg: 'isStaticSource', 
        truthy: false 
      }
    },
    types: { 
      control: 'check', 
      options: ['projects', 'proposals'], 
      description: 'The types of projects to display.',
      if: { 
        arg: 'isStaticSource', 
        truthy: false 
      }
    }, 
    pageSize: { 
      control: 'number', 
      description: 'The number of projects to display at a time.' 
    },
    isStaticSource: { 
      control: 'boolean', 
      description: 'Toggle whether or not the data is coming from a static source. (e.g. a static JSON file)',
    },
    url: { 
      control: 'text', 
      description: 'The URL to the data source.',
      if: { 
        arg: 'isStaticSource' 
      }
    },
    displayCategories: {
      control: 'boolean',
      description: 'Displays the search bar and category filters.'
    },
  },
  args: {
    workingGroup: workingGroups[0],
    isStaticSource: false,
    displayCategories: false,
  },
}

export const Cards: Story = {
  ...commonOptions,
  render: ({ workingGroup, types, pageSize, isStaticSource, displayCategories }) => {
    afterStoryRender(eclipsefdnProjects.render);

    return html`
<div 
  class="featured-projects list-inline"
  ${workingGroup ? `data-url="https://projects.eclipse.org/api/projects?working_group=${workingGroup}"` : ''}
  ${setOptionalAttribute('data-types', types)}
  ${setOptionalAttribute('data-is-static-source', isStaticSource)}
  ${setOptionalAttribute('data-page-size', pageSize)}
  ${setOptionalAttribute('data-display-categories', displayCategories)}
>
</div>
    `
  },
}

export const List: Story = {
  ...commonOptions,
  render: ({ workingGroup, types, pageSize, isStaticSource, displayCategories }) => {
    afterStoryRender(eclipsefdnProjects.render);

    return html`
<div 
  class="featured-projects"
  data-template-id="list"
  ${workingGroup ? `data-url="https://projects.eclipse.org/api/projects?working_group=${workingGroup}"` : ''}
  ${setOptionalAttribute('data-types', types)}
  ${setOptionalAttribute('data-is-static-source', isStaticSource)}
  ${setOptionalAttribute('data-page-size', pageSize)}
  ${setOptionalAttribute('data-display-categories', displayCategories)}
>
</div>
    `
  },
}


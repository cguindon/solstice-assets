/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import type { Meta, StoryObj } from '@storybook/html';
import { container, afterStoryRender, html } from '../utils';
import { renderVideos } from '../../js/privacy/eclipsefdn.videos';

const meta: Meta = {
  title: 'Widgets/Video',
  decorators: [container()],
};

export default meta;

type Story = StoryObj<any>;

export const VideoEmbed: Story = {
  argTypes: {
    embedUrl: { 
      control: 'text', 
      description: 'The video or playlist embed url from YouTube',
    },
    thumbnailResolution: {
      control: 'select',
      description: 'The resolution of the thumbnail when no cookies are set (deprecated)',
      options: ['sd', 'hq', 'max'],
    },
  },
  args: {
    embedUrl: '//www.youtube.com/embed/j1q009o37Ik',
  },
  render: ({ embedUrl, thumbnailResolution }) => {
    // No types for eclipsefdnVideos yet.
    afterStoryRender(renderVideos);

    return html`
      <a class="eclipsefdn-video" href="${embedUrl}" data-thumbnail-resolution="${thumbnailResolution}"></a>
    `;
  },
}

export const PlaylistEmbed: Story = {
  argTypes: {
    embedUrl: { 
      control: 'text', 
      description: 'The video or playlist embed url from YouTube',
    },
  },
  args: {
    embedUrl: '//www.youtube.com/embed/playlist?list=PLy7t4z5SYNaQz_NGqvYsWpAhezXbexPKL',
  },
  render: ({ embedUrl }) => {
    // No types for eclipsefdnVideos yet.
    afterStoryRender(renderVideos);

    return html`
      <a class="eclipsefdn-video" href="${embedUrl}"></a>
    `;
  },
}


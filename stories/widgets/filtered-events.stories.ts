/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { StoryObj } from '@storybook/html';
import { container, setOptionalAttribute, publishTargets } from '../utils';
import '../../js/api/jquery.eclipsefdn.api';

export default {
  title: 'Widgets/Filtered Events',
  decorators: [container()],
};

type Story = StoryObj<any>;

export const FilteredEvents: Story = {
  argTypes: {
    id: { control: 'text' },
    publishTarget: { control: 'select', options: publishTargets },
    count: { control: 'number' },
    pagination: { control: 'boolean' },
  },
  args: {
    id: 'filtered-events',
    publishTarget: 'eclipse_org',
    count: 4,
    pagination: true,
  },
  render: ({ id, publishTarget, count, pagination }) => {
    return (`
      <div 
        id="${id}" 
        data-publish-target="${publishTarget}"
        ${setOptionalAttribute('data-count', count)}
        ${setOptionalAttribute('data-pagination', pagination)}
      >
      </div>
      <script>
        (function(e) {
          e('#${id}').eclipseFdnApi({ type: 'filteredEvents' }) 
        })(jQuery, document)
      </script>
    `)
  },
}

/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import '../../less/astro/main.less';

import type { Meta, StoryObj } from '@storybook/html';
import { container, afterStoryRender, setOptionalAttribute, publishTargets, html } from '../utils';
import eclipsefdnNewsroomResources from '../../js/solstice/eclipsefdn.newsroom-resources';

const meta: Meta = {
  title: 'Widgets/Newsroom Resources',
  tags: ['autodocs'],
  decorators: [container()],
};

export default meta;

type Story = StoryObj<any>;

const commonOptions: Partial<Story> = {
  argTypes: {
    type: {
      control: 'check',
      options: ['white_paper', 'case_study', 'social_media_kit', 'survey_report'],
      description: 'The newsroom resource types to display.',
    },
    publishTarget: { control: 'select', options: publishTargets },
    title: {
      control: 'text',
      description: 'The heading text.'
    },
    pageSize: {
      control: 'number',
      description: 'The number of newsroom resource items to display.',
    },
    skip: {
      control: 'number',
      description: 'The number of newsroom resource items to skip from the beginning.',
    },
  },
  args: {
    title: 'Newsroom Resources', 
    pageSize: 3,
  },
}

export const Cover: Story = {
  ...commonOptions,
  render: ({ type, publishTarget, title, pageSize, skip }) => {
    afterStoryRender(eclipsefdnNewsroomResources.render);

    return html`
<div 
  class="newsroom-resources"
  ${setOptionalAttribute('data-res-wg', publishTarget)}
  ${setOptionalAttribute('data-res-type', type?.join(', '))}
  ${setOptionalAttribute('data-res-title', title)}
  ${setOptionalAttribute('data-page-size', pageSize)}
  ${setOptionalAttribute('data-res-skip', skip)}
  data-res-template="cover"
>
</div>
    `
  },
}


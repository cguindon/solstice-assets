/*!
 * Copyright (c) 2018 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// This is the webpack config for /docs/legacy

// Configure
require('./webpack-solstice-assets.mix');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();
mix.sourceMaps();

// Paths
mix.setPublicPath('docs/legacy/dist');
mix.setResourceRoot('../');

// Copy the logo from src'
mix.copy(
  'images/logo/eclipse-foundation-white-orange.svg',
  'docs/legacy/dist/images/logo/eclipse-foundation-white-orange.svg'
);
mix.copy(
  'images/template/placeholders',
  'docs/legacy/dist/images/template/placeholders'
);
mix.copy('images/**/*', 'docs/legacy/dist/images');

// Default CSS
mix.less('less/astro/main.less', 'docs/legacy/dist/css/astro.css');
mix.less('less/quicksilver/styles.less', 'docs/legacy/dist/css/quicksilver.css');
mix.less('less/quicksilver/jakarta/styles.less', 'docs/legacy/dist/css/jakarta.css');
mix.less(
  'less/quicksilver/eclipse-ide/styles.less',
  'docs/legacy/dist/css/eclipse-ide.css'
);
mix.less('less/solstice/_barebone/styles.less', 'docs/legacy/dist/css/barebone.css');
mix.less(
  'less/solstice/_barebone/footer.less',
  'docs/legacy/dist/css/barebone-footer.css'
);
mix.less('less/solstice/table.less', 'docs/legacy/dist/css/table.css');
mix.less('less/solstice/styles.less', 'docs/legacy/dist/css/solstice.css');

// Copy cookieconsent files
mix.copy(
  'node_modules/cookieconsent/build/cookieconsent.min.css',
  'docs/legacy/dist/css/vendor/cookieconsent.min.css'
);
mix.copy(
  'node_modules/cookieconsent/src/cookieconsent.js',
  'docs/legacy/dist/js/vendor/cookieconsent.min.js'
);

mix.js(['js/main.js'], 'docs/legacy/dist/js/solstice.js');
mix.js(['js/astro.js'], 'docs/legacy/dist/js/astro.js');

// Standalone JS
mix.js(['js/solstice/eclipsefdn.promotion.js'], 'docs/legacy/dist/js/eclipsefdn.promotion.js');
mix.js(['js/solstice/eclipsefdn.members-list.js'], 'docs/legacy/dist/js/eclipsefdn.members-list.js');

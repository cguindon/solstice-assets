/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/** @type {import('jest').Config} */
const config = {
    collectCoverage: true,
    collectCoverageFrom: [
      'js/**/*.js',
      'js/**/*.ts',
      'js/**/*.mustache',
    ],
    coveragePathIgnorePatterns: [
      'mocks',
      'tests',
    ],
    testMatch: [
      '**/tests/api-test-suite.test.js',
      '**/tests/ui-test-suite.test.js',
      '**/tests/shared-test-suite.test.js',
    ],
    testPathIgnorePatterns: [
      '<rootDir>/node_modules',
      '<rootDir>/docs'
    ],
    transform: {
      '^.+\\.(j|t)sx?$': ['babel-jest', { configFile: './babel.test.json' }],
      '^.+\\.mustache$': '<rootDir>/build/jest/mustache-transformer.js',
    },
}

module.exports = config;

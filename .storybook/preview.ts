/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import type { Preview } from '@storybook/html';
import $ from 'jquery';
import '../js/privacy/eclipsefdn.cookie-consent';

const preview: Preview = {
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    options: {
      storySort: {
        order: ['Getting Started', 'Core', 'Static Components', 'Widgets', 'Layout'],
      },
    },
    docs: {
      toc: true,
    },
    chromatic: {
      disable: true
    },
    html: {
      root: '#container',
    },
  },
};

//@ts-ignore
window.jQuery = $;
//@ts-ignore
window.$ = $;

export default preview;

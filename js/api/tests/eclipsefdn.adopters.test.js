/*
 * Copyright (c) 2022, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import getProjectsWithAdopters from '../eclipsefdn.adopters';
import { getMockProjectsFromWorkingGroup } from './mocks/adopters/mock-data';

describe('Adopters API', () => {
    it('Fetches projects with adopters', async () => {
        const result = await getProjectsWithAdopters('internet-things-iot');

        expect(result).not.toBeNull();
        expect(result).not.toHaveLength(0);
        expect(result[0]).toHaveProperty('adopters');
    });

    it('Fetches correct number of projects with adopters', async () => {
      // Tests whether the link header fetching works
      const result = await getProjectsWithAdopters('internet-things-iot');
      const numberOfProjectsWithAdopters = getMockProjectsFromWorkingGroup('internet-things-iot').model.length; 

      expect(result).toHaveLength(numberOfProjectsWithAdopters);
    });
});

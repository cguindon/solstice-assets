/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { setupServer } from 'msw/node';
import adoptersHandlers from './adopters/handlers';
import interestGroupsHandlers from './interest-groups/handlers';
import mediaLinkHandlers from './media-link/handlers';
import membershipHandlers from './membership/handlers';
import newsroomHandlers from './newsroom/handlers';
import planetEclipseHandlers from './planet-eclipse/handlers';
import projectsHandlers from './projects/handlers';

export const server = setupServer(
  ...adoptersHandlers,
  ...interestGroupsHandlers,
  ...mediaLinkHandlers,
  ...membershipHandlers,
  ...newsroomHandlers,
  ...planetEclipseHandlers,
  ...projectsHandlers
);

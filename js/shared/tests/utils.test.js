/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { isWorkingGroup, isInterestGroup, getCollaborationType, stringToBoolean } from '../utils';

describe('Utilities', () => {
  describe('stringToBoolean ', () => {
    it('Should return true if "true" was passed', () => {
      const result = stringToBoolean('true');
      expect(result).toEqual(true);
    });

    it('Should return false if "false" was passed', () => {
      const result = stringToBoolean('false');
      expect(result).toEqual(false);
    });

    it('Should ignore case', () => {
      const results = [
        stringToBoolean('TRUE'), 
        stringToBoolean('True'), 
        stringToBoolean('FALSE'), 
        stringToBoolean('False')
      ];

      expect(results.every((r) => typeof r === 'boolean'));
    });

    it('Should throw a TypeError if anything other than "false" or "true" was passed.', () => {
      // To test for errors, we need to wrap the function inside another
      // function.
      let result = () => stringToBoolean('something invalid');
      expect(result).toThrow(TypeError)

      result = () => stringToBoolean(undefined);
      expect(result).toThrow(TypeError);

      result = () => stringToBoolean(null);
      expect(result).toThrow(TypeError);
    });
  });

  describe('isWorkingGroup', () => {
    it('Should return true if argument has fields in common with a working group.', () => {
      const obj = {
        alias: 'Alias',
        title: 'Title',
        status: 'Status',
        description: 'Description',
      };

      const result = isWorkingGroup(obj);
      expect(result).toEqual(true);
    });

    it('Should return false if argument is missing fields from a working group.', () => {
      // Purposely missing "alias".
      const obj = {
        title: 'Title',
        status: 'Status',
        description: 'Description',
      };

      const result = isWorkingGroup(obj);
      expect(result).toEqual(false);
    });
  });

  describe('isInterestGroup', () => {
    it('Should return true if argument has fields in common with an interest group.', () => {
      const obj = {
        id: 'ID',
        shortProjectId: 'Short Project ID',
        projectId: 'Project ID',
        state: 'State',
      };

      const result = isInterestGroup(obj);
      expect(result).toEqual(true);
    });

    it('Should return false if argument is missing fields from an interest group.', () => {
      // Purposely missing "shortProjectId".
      const obj = {
        id: 'ID',
        projectId: 'Project ID',
        state: 'State',
      };

      const result = isInterestGroup(obj);
      expect(result).toEqual(false);
    });
  });

  describe('getCollaborationType', () => {
    it('Should return "interest-group" if argument is an interest group', () => {
      const interestGroup = {
        id: 'ID',
        shortProjectId: 'Short Project ID',
        projectId: 'Project ID',
        state: 'State',
      };

      const result = getCollaborationType(interestGroup);
      expect(result).toEqual('interest-group');
    });

    it('Should return "working-group" if argument is a working group', () => {
      const workingGroup = {
        alias: 'Alias',
        title: 'Title',
        status: 'Status',
        description: 'Description',
      };

      const result = getCollaborationType(workingGroup);
      expect(result).toEqual('working-group');
    });

    it('Should return "invalid" if argument is not a collaboration', () => {
      const obj = {
        name: 'Product Name',
        price: 15.99
      };

      const result = getCollaborationType(obj);
      expect(result).toEqual('invalid');
    });
  });
});

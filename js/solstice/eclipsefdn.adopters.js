/*!
 * Copyright (c) 2019, 2022, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Eric Poirier <eric.poirier@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import defaultTemplate from './templates/adopters.mustache';
import getProjectsWithAdopters from '../api/eclipsefdn.adopters';
import { scrollToAnchor } from './utils';
import renderTemplate from './templates/render-template';

const defaultOptions = {
  srcRoot: 'https://api.eclipse.org/adopters', 
  srcProjectPrefix: '/projects',
  workingGroup: '',
  templateId: 'default',
  logoWhite: false
};

const templates = {
  default: defaultTemplate
} 

const render = async () => {
  const element = document.querySelector('.eclipsefdn-adopters');
  if (!element) return;

  const options = { ...defaultOptions, ...element.dataset };

  const projects = await getProjectsWithAdopters(options.workingGroup);

  const data = { projects, srcRoot: options.srcRoot, isWhiteLogo: options.logoWhite };

  await renderTemplate({ 
    element, 
    data,
    templates, 
    templateId: options.templateId, 
  });

  if (location.hash) scrollToAnchor();

  return;
};

export default { render };

/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import eclipsefdnMembersDetail from '../eclipsefdn.members-detail';
import * as membershipAPIModule from '../../api/eclipsefdn.membership';
import * as workingGroupsAPIModule from '../../api/eclipsefdn.working-groups';
import * as interestGroupsAPIModule from '../../api/eclipsefdn.interest-groups';
import { mockOrganizations, mockOrganizationProducts, mockOrganizationProjects } from '../../api/tests/mocks/membership/mock-data';
import { mockInterestGroups } from '../../api/tests/mocks/interest-groups/mock-data';
import { wait } from '../utils';

/*
 * Mutates the search params from `window.location`. This mutates
 * `window.location`, so do not forget to call `cleanup` after your tests.
 */
const mockSearchParams = (searchParams) => {
  // Keep track of the original window.location so we can cleanup our mock
  // afterwards.
  if (!searchParams || !searchParams instanceof URLSearchParams) {
    throw new TypeError('Invalid search params provided');
  }
  const originalLocation = window.location;

  delete window.location;
  window.location = new URL('http://localhost:8080?' + searchParams.toString());
  
  // Returns a cleanup function to reset the mock.
  return () => {
    window.location = originalLocation;
  };
};

let getOrganizationByIdSpy;
let getOrganizationProductsSpy;
let getOrganizationProjectsSpy;
let getOrganizationWorkingGroupsSpy;
let getOrganizationInterestGroupsSpy;

const useOrganizationMock = (options) => {
  let organization;

  if (options?.level) {
    const index = mockOrganizations.api.findIndex((org) => org.levels.find((level) => level.level === options.level));
    organization = { 
      api: mockOrganizations.api.at(index), 
      model: mockOrganizations.model.at(index) 
    };
  } else {
    // If we simply need an organization without any additional criteria,
    // return the first mock.
    organization = {
      api: mockOrganizations.api.at(0),
      model: mockOrganizations.model.at(0),
    };
  }

  const products = {
    api: mockOrganizationProducts.api[organization.model.organizationId],
    model: mockOrganizationProducts.model[organization.model.organizationId],
  }

  const projects = {
    api: mockOrganizationProjects.api[organization.model.organizationId],
    model: mockOrganizationProjects.model[organization.model.organizationId],
  };

  const workingGroups = {
    api: organization.api.wgpas.map((wgpa) => wgpa.working_group),
    model: organization.model.wgpas.map((wgpa) => wgpa.workingGroup),
  }

  // Mutate the interest groups to contain at least one participant from the
  // organization.
  let interestGroups = JSON.parse(JSON.stringify(mockInterestGroups)); // Deep copy to prevent mutating the mock data which could be used by other tests.
  mockInterestGroups.api.forEach((interestGroup) => interestGroup.participants.at(0).organization.id = organization.model.organizationId);
  mockInterestGroups.model.forEach((interestGroup) => interestGroup.participants.at(0).organization.id = organization.model.organizationId);

  getOrganizationByIdSpy.mockImplementation(() => [organization.model, null]);
  getOrganizationProductsSpy.mockImplementation(() => [products.model, null]);
  getOrganizationProjectsSpy.mockImplementation(() => [projects.model, null]);
  getOrganizationWorkingGroupsSpy.mockImplementation(() => [workingGroups.model, null]);
  getOrganizationInterestGroupsSpy.mockImplementation(() => [interestGroups.model, null]);

  return { organization, products, projects, interestGroups };
}

describe('eclipsefdn-members-detail', () => {
  beforeEach(() => {
    getOrganizationByIdSpy = jest.spyOn(membershipAPIModule, 'getOrganizationById');
    getOrganizationProductsSpy = jest.spyOn(membershipAPIModule, 'getOrganizationProducts');
    getOrganizationProjectsSpy = jest.spyOn(membershipAPIModule, 'getOrganizationProjects');
    getOrganizationWorkingGroupsSpy = jest.spyOn(workingGroupsAPIModule, 'getOrganizationWorkingGroups');
    getOrganizationInterestGroupsSpy = jest.spyOn(interestGroupsAPIModule, 'getOrganizationInterestGroups');
  });

  it('Should display error message for missing member_id', async () => {
    document.body.innerHTML = `
      <div class="member-detail"></div>
    `; 

    await eclipsefdnMembersDetail.render();

    const h1 = document.querySelector('h1');
    expect(h1.innerHTML).toEqual('Invalid Member Id');
  });

  it('Should display error message for non-numeric member_id', async () => {
    const params = new URLSearchParams();
    params.set('member_id', 'abcdefg');
    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();
    
    const h1 = document.querySelector('h1');
    expect(h1.innerHTML).toEqual('Invalid Member Id');

    cleanup();
  });

  it('Should display error message when no organization found for valid member_id', async () => {
    const params = new URLSearchParams();
    params.set('member_id', '123');
    const cleanup = mockSearchParams(params);

    // Simulate a 404 from the Membership API
    getOrganizationByIdSpy.mockImplementation(() => [null, { message: 'Could not find member', code: 404 }]);
    
    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    const h1 = document.querySelector('h1');
    expect(h1.innerHTML).toEqual('Invalid Member Id');

    cleanup();
  });

  it('Should render member detail for existing organization', async () => {
    const { organization } = useOrganizationMock();

    const params = new URLSearchParams();
    params.set('member_id', organization.model.organizationId);
    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    const logoLink = document.querySelector('.member-detail-logo-link');
    expect(logoLink.getAttribute('title')).toEqual(organization.model.name);

    cleanup();
  });

  it('Should display SD membership image for organization', async () => {
    const { organization } = useOrganizationMock({ level: 'SD' });

    const params = new URLSearchParams();
    params.set('member_id', organization.model.organizationId);

    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    const levelImg = document.querySelector('.member-detail-level-img');
    expect(levelImg.getAttribute('src').includes('strategic-members.png')).toBe(true);

    cleanup();
  });

  it('Should display AP membership image for organization', async () => {
    const { organization } = useOrganizationMock({ level: 'AP' });

    const params = new URLSearchParams();
    params.set('member_id', organization.model.organizationId);
    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    const levelImg = document.querySelector('.member-detail-level-img');
    expect(levelImg.getAttribute('src').includes('contributing-members.png')).toBe(true);

    cleanup();
  });

  it('Should display AS membership image for organization', async () => {
    const { organization } = useOrganizationMock({ level: 'AS' });

    const params = new URLSearchParams();
    params.set('member_id', organization.model.organizationId);
    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    const levelImg = document.querySelector('.member-detail-level-img');
    expect(levelImg.getAttribute('src').includes('associate-members.png')).toBe(true);

    cleanup();
  });

  it('Should show links sidebar for organization with products and projects', async () => {
    const { organization, products, projects } = useOrganizationMock();

    const params = new URLSearchParams();
    params.set('member_id', organization.model.organizationId);
    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    const productLinks = Array.from(document.querySelectorAll('.member-detail-product a'));
    const projectLinks = Array.from(document.querySelectorAll('.member-detail-project a'));

    // Expect all product link hrefs to correspond to a product from the
    // organization's products
    expect(
      productLinks.every((link) => 
        products.model.find((product) => 
          product.url === link.getAttribute('href')
        )
      )
    ).toBe(true);
    
    // Expect the same amount of product links in the UI as there are in the
    // data.
    expect(productLinks.length)
      .toEqual(mockOrganizationProducts.model[organization.model.organizationId].length);

    expect(
      projectLinks.every((link) =>
        projects.model.find((project) =>
          `https://projects.eclipse.org/projects/${project.id}` === link.getAttribute('href')
        )
      )
    ).toBe(true);

    cleanup();
  });

  it('Should show working groups associated with an organization', async () => {
    const { organization } = useOrganizationMock();

    const params = new URLSearchParams();
    params.set('member_id', organization.model.organizationId);
    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    // Create a set of working groups that have been rendered on the page.
    const workingGroups = new Set(Array
      .from(document.querySelectorAll('.member-detail-working-group-list img, .member-detail-working-group-list .logo-placeholder-text'))
      // The alt text would include the working group name.
      .map((element) => element.getAttribute('alt') ?? element.innerHTML));

    expect(workingGroups.size).not.toBe(0);
    // Expect every working group within the mock organization to be found in
      // the set of rendered working groups.
    expect(
      organization.api.wgpas
        .map((wgpa) => wgpa.working_group)
        .every((workingGroup) => workingGroups.has(workingGroup))
    )

    cleanup();
  });

  it('Should show interest groups associated with an organization', async () => {
    const { organization, interestGroups } = useOrganizationMock();

    const params = new URLSearchParams();
    params.set('member_id', organization.model.organizationId);
    const cleanup = mockSearchParams(params);

    document.body.innerHTML = `
      <div class="member-detail"></div>
    `;

    await eclipsefdnMembersDetail.render();

    // Create a set of working groups that have been rendered on the page.
    const renderedInterestGroups = new Set(Array
      .from(document.querySelectorAll('.member-detail-interest-group-list img, .member-detail-interest-group-list .logo-placeholder-text'))
      // The alt text would include the working group name.
      .map((element) => element.getAttribute('alt') ?? element.innerHTML));

    expect(renderedInterestGroups.size).not.toBe(0);
    // Expect every interest group within the mock organization to be found in
    // the set of rendered interest groups.
    expect(
      interestGroups.model.every((interestGroup) => renderedInterestGroups.has(interestGroup.title))
    )

    cleanup();
  });

  // it('Should not show links sidebar for organization without products or projects', () => {});
  // it('Should display formatted member since date', () => {});
  // it('Should render long description without line breaks', () => {});
  // it('Should validate URL and return false for invalid website', () => {});
  // it('Should validate URL and return true for valid website', () => {});
});

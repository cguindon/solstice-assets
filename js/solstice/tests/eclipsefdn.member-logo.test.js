/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import eclipsefdnMemberLogo from '../eclipsefdn.member-logo';
import * as membershipModule from '../../api/eclipsefdn.membership';

describe('eclipsefdn-member-logo', () => {
  let getOrganizationByIdSpy;

  beforeEach(() => {
    getOrganizationByIdSpy = jest.spyOn(membershipModule, 'getOrganizationById');
  });

  it('Do not render member logo without organization id and log error', async () => {
    const consoleSpy = jest.spyOn(console, 'error').mockImplementation(() => {});

    document.querySelector('body').innerHTML = `
      <div class="eclipsefdn-member-logo"></div>
    `;
    
    await eclipsefdnMemberLogo.render();
    
    expect(document.querySelector('.eclipsefdn-member-logo').children.length).toBe(0);
    expect(consoleSpy).toHaveBeenCalledWith('No organization id provided to eclipsefdn-member-logo');
  });

  // Skipping due to template data not affecting template. Possible bug.
  it.skip('Render member logo', async () => {
    getOrganizationByIdSpy.mockReturnValue([{ name: 'IBM', logos: { web: 'https://example.com/ibm.png' }}, null]);

    document.querySelector('body').innerHTML = `
      <div class="eclipsefdn-member-logo" data-id="656"></div>
    `;
    
    await eclipsefdnMemberLogo.render();
    
    expect(getOrganizationByIdSpy).toHaveBeenCalledWith(656);
    expect(document.querySelector('img').src).toBe('https://example.com/ibm.png');
  });
});


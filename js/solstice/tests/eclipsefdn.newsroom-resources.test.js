/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import eclipsefdnNewsroomResources from '../eclipsefdn.newsroom-resources';
import * as newsroomAPIModule from '../../api/eclipsefdn.newsroom';
import { mockNewsroomResources } from '../../api/tests/mocks/newsroom/mock-data';

describe('eclipsefdn-newsroom-resources', () => {
  let getNewsroomResourcesSpy;

  beforeEach(() => {
    getNewsroomResourcesSpy = jest.spyOn(newsroomAPIModule, 'getNewsroomResources');
  });

  it('Template "list": Renders the correct number of resource items when the page size is set to 3', async () => {
    // It is expected that there will be 3 items for each resouce type
    getNewsroomResourcesSpy.mockReturnValue([mockNewsroomResources, null]);

    document.querySelector('body').innerHTML = `
      <div
        class="newsroom-resources"
        data-res-type="case_study, white_paper"
        data-page-size="3"
        data-res-title="Case Studies, White Papers"
        data-res-template="list"
      >
      </div>
    `;

    await eclipsefdnNewsroomResources.render();
    
    // The `getNewsroomResources` function should have been called twice.
    // Once for each resource type. Both calls should have requested 3 items.
    expect(getNewsroomResourcesSpy).toHaveBeenNthCalledWith(1, expect.objectContaining({ pageSize: 3 }));
    expect(getNewsroomResourcesSpy).toHaveBeenNthCalledWith(2, expect.objectContaining({ pageSize: 3 }));
  });

  it('Template "cover": Renders the correct number of resource items when page size is set to 3', async () => {
    getNewsroomResourcesSpy.mockReturnValue([mockNewsroomResources, null]);

    document.querySelector('body').innerHTML = `
      <div 
        class="newsroom-resources" 
        data-page-size="3" 
        data-res-title="Case Studies"
        data-res-template="cover"
        data-res-view-more="https://iot.eclipse.org/case-studies/"
      >
      </div>
    `;

    await eclipsefdnNewsroomResources.render();

    expect(getNewsroomResourcesSpy).toHaveBeenCalledWith(
      expect.objectContaining({ pageSize: 3 })
    );
  });

  it('Template "cover": Renders title', async () => {
    getNewsroomResourcesSpy.mockReturnValue([mockNewsroomResources, null]);

    const title = 'Case Studies'
    document.querySelector('body').innerHTML = `
      <div 
        class="newsroom-resources" 
        data-page-size="3" 
        data-res-title="${title}"
        data-res-type="case_study" 
        data-res-wg="eclipse_iot"
        data-res-template="cover"
        data-res-view-more="https://iot.eclipse.org/case-studies/"
      >
      </div>
    `;

    await eclipsefdnNewsroomResources.render();

    const newsroomResourcesTitleElement = document.querySelector('.newsroom-resource-section-title');
    expect(newsroomResourcesTitleElement.textContent).toBe(title);
  });
});

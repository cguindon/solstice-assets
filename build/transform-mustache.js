/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

module.exports = function(sourceText, sourcePath) {
  const isPartial = sourcePath.endsWith('.partial.mustache');

  // Handle partials as strings. We do not precompile them.
  if (isPartial) return `module.exports = ${JSON.stringify(sourceText)}`;

  // Precompile the contents of a mustache file and return a function which
  // triggers the render.
  return `
module.exports = function() {
  const Hogan = require('hogan.js');
  const precompiledTemplate = Hogan.compile(${JSON.stringify(sourceText)});

  return (...args) => precompiledTemplate.render(...args);
}();
  `;
}
